package cz.cvut.fit.tjv.pharmacheck.frontend.domain.prescription;

import java.util.Objects;
import java.util.UUID;

public class PrescriptionSimpleDto {

    private UUID EPrescriptionCode;

    public UUID getEPrescriptionCode() {
        return EPrescriptionCode;
    }

    public void setEPrescriptionCode(UUID EPrescriptionCode) {
        this.EPrescriptionCode = EPrescriptionCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrescriptionSimpleDto that = (PrescriptionSimpleDto) o;
        return Objects.equals(EPrescriptionCode, that.EPrescriptionCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(EPrescriptionCode);
    }
}
