package cz.cvut.fit.tjv.pharmacheck.frontend.service;

import cz.cvut.fit.tjv.pharmacheck.frontend.api_client.DrugstoreClient;
import org.springframework.stereotype.Service;

@Service
public class DrugstoreService {

    private final DrugstoreClient drugstoreClient;

    public DrugstoreService(DrugstoreClient drugstoreClient) {
        this.drugstoreClient = drugstoreClient;
    }
}
