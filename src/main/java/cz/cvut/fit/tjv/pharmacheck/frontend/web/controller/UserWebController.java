package cz.cvut.fit.tjv.pharmacheck.frontend.web.controller;

import cz.cvut.fit.tjv.pharmacheck.frontend.api_client.PrescriptionClient;
import cz.cvut.fit.tjv.pharmacheck.frontend.api_client.UserClient;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.user.UserDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.web.model.UserModel;
import cz.cvut.fit.tjv.pharmacheck.frontend.web.model.converter.UserConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/users")
public class UserWebController {

    private final UserClient userClient;

    private final PrescriptionClient prescriptionClient;

    public UserWebController(UserClient userClient, PrescriptionClient prescriptionClient) {

        this.userClient = userClient;
        this.prescriptionClient = prescriptionClient;
    }

    @GetMapping
    public String list(Model model) {
        model.addAttribute("users", userClient.readAll() );
        return "users/list";
    }

    @PostMapping("/delete")
    public String delete(@RequestParam Long id, Model model) {
        userClient.setCurrentUser(id);
        userClient.deleteOne();
        model.addAttribute("users", userClient.readAll() );
        return "users/list";
    }

    @GetMapping("/add")
    public String addUser(Model model) {
        model.addAttribute("userModel", new UserModel());
        return "users/add";
    }


    @PostMapping("/add")
    public String addUserSubmit(@ModelAttribute UserModel data, Model model) {

        try {
            UserDto user = userClient.create( UserConverter.userModelToUserSimpleDto( data ) );
            model.addAttribute("success", true);
            model.addAttribute("successMessage", "User successfully created with ID: " + user.getId());
            model.addAttribute("userModel", user);
        }
        catch (Exception e) {
            model.addAttribute("success", false);
            model.addAttribute("errorMessage", e.getMessage());
            model.addAttribute("userModel", data);
        }
        return "users/add";
    }

    @GetMapping("/detail")
    public String detail(@RequestParam Long id, Model model) {
        userClient.setCurrentUser(id);
        model.addAttribute("user", userClient.readOne());
        return "users/detail";
    }

    @GetMapping("/edit")
    public String editUser( @RequestParam Long id, Model model ) {
        userClient.setCurrentUser(id);
        model.addAttribute("userModel", new UserModel());
        model.addAttribute("user", userClient.readOne());
        return "users/edit";
    }

    @PostMapping("/edit")
    public String editUserSubmit( @RequestParam Long id, @ModelAttribute UserModel data, Model model ) {

        userClient.setCurrentUser(id);

        try {
            userClient.updateOne( UserConverter.userModelToUserSimpleDto( data ) );
            model.addAttribute("success", true);
            model.addAttribute("successMessage", "User successfully saved!");
        }
        catch (Exception e) {
            model.addAttribute("success", false);
            model.addAttribute("errorMessage", "There was a problem saving the user!");
        }

        model.addAttribute("user", userClient.readOne());

        return "users/detail";
    }

    @PostMapping("/removePrescription")
    public String removePrescription( @RequestParam Long id, @RequestParam Long prescriptionID, Model model ) {

        userClient.setCurrentUser(id);

        prescriptionClient.setCurrentPrescription( prescriptionID );
        prescriptionClient.removeUser();

        model.addAttribute("user", userClient.readOne());
        return "users/detail";
    }



}
