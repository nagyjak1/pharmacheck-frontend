package cz.cvut.fit.tjv.pharmacheck.frontend.web.model.converter;

import cz.cvut.fit.tjv.pharmacheck.frontend.domain.user.UserSimpleDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.web.model.UserModel;

public class UserConverter {

    public static UserSimpleDto userModelToUserSimpleDto(UserModel data) {
        UserSimpleDto dto = new UserSimpleDto();
        dto.setUsername( data.getUsername() );
        return dto;
    }
}
