package cz.cvut.fit.tjv.pharmacheck.frontend.api_client;

import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drug.DrugDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drugstore.DrugstoreDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.user.UserDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.user.UserSimpleDto;
import org.glassfish.jersey.logging.LoggingFeature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;


@Component
public class UserClient {
    private final WebTarget usersEndpoint;
    private final WebTarget singleEndpointTemplate;
    private WebTarget singleUserEndpoint;

    public UserClient(@Value("${backend.url}") String apiUrl) {
        var c = ClientBuilder.newClient().register(LoggingFeature.builder().level(Level.ALL).build());
        usersEndpoint = c.target(apiUrl + "/users");
        singleEndpointTemplate = usersEndpoint.path("/{id}");
    }

    public Collection<UserDto> readAll() {
        var res = usersEndpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .get(UserDto[].class);
        return Arrays.asList(res);
    }

    public UserDto create(UserSimpleDto e) {
        return usersEndpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(e, MediaType.APPLICATION_JSON_TYPE), UserDto.class);
    }

    public void setCurrentUser(long id) {
        singleUserEndpoint = singleEndpointTemplate.resolveTemplate("id", id);
    }

    public void deleteOne() {
        singleUserEndpoint.request(MediaType.APPLICATION_JSON_TYPE).delete();
    }

    public UserDto readOne() {
        return singleUserEndpoint.request(MediaType.APPLICATION_JSON_TYPE).get(UserDto.class);
    }

    public void updateOne(UserSimpleDto e) {
        singleUserEndpoint.request(MediaType.APPLICATION_JSON_TYPE).put(Entity.entity(e, MediaType.APPLICATION_JSON_TYPE), UserDto.class);
    }

}
