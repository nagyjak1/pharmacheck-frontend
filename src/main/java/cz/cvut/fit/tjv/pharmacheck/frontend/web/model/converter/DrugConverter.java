package cz.cvut.fit.tjv.pharmacheck.frontend.web.model.converter;

import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drug.DrugDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drug.DrugSimpleDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.web.model.DrugModel;

public class DrugConverter {

    public static DrugSimpleDto drugModelToDrugSimpleDto (DrugModel drugModel ) {
        DrugSimpleDto dto = new DrugSimpleDto();
        dto.setName( drugModel.getName() );
        dto.setDescription( drugModel.getDescription());
        return dto;
    }

}
