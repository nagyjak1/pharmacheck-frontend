package cz.cvut.fit.tjv.pharmacheck.frontend.service;

import cz.cvut.fit.tjv.pharmacheck.frontend.api_client.DrugClient;
import org.springframework.stereotype.Service;

@Service
public class DrugService {

    private final DrugClient drugClient;

    public DrugService(DrugClient drugClient) {
        this.drugClient = drugClient;
    }
}
