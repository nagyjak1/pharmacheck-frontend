package cz.cvut.fit.tjv.pharmacheck.frontend.domain.drug;

import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drugstore.DrugstoreWithoutDrugDto;

import java.util.Collection;
import java.util.Objects;

public class DrugWithoutPrescriptionDto {

    private Long id;

    private String name;

    private String description;

    private Collection<DrugstoreWithoutDrugDto> drugstores;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<DrugstoreWithoutDrugDto> getDrugstores() {
        return drugstores;
    }

    public void setDrugstores(Collection<DrugstoreWithoutDrugDto> drugstores) {
        this.drugstores = drugstores;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrugWithoutPrescriptionDto that = (DrugWithoutPrescriptionDto) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(description, that.description) && Objects.equals(drugstores, that.drugstores);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, drugstores);
    }
}
