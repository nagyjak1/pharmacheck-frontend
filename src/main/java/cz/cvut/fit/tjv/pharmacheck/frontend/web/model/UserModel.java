package cz.cvut.fit.tjv.pharmacheck.frontend.web.model;

public class UserModel {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
