package cz.cvut.fit.tjv.pharmacheck.frontend.web.model.converter;

import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drugstore.DrugstoreSimpleDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.web.model.DrugstoreModel;

public class DrugstoreConverter {

    public static DrugstoreSimpleDto drugstoreModelToDrugstoreSimpleDto(DrugstoreModel drugModel) {
        DrugstoreSimpleDto dto = new DrugstoreSimpleDto();
        dto.setCity( drugModel.getCity() );
        return dto;
    }
}
