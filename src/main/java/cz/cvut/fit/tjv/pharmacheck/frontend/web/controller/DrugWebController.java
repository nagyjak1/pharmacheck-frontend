package cz.cvut.fit.tjv.pharmacheck.frontend.web.controller;

import cz.cvut.fit.tjv.pharmacheck.frontend.api_client.DrugClient;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drug.DrugDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.web.model.DrugModel;
import cz.cvut.fit.tjv.pharmacheck.frontend.web.model.converter.DrugConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/drugs")
public class DrugWebController {

    private final DrugClient drugClient;

    public DrugWebController(DrugClient drugClient) {
        this.drugClient = drugClient;
    }

    @GetMapping
    public String list(Model model) {
        model.addAttribute("drugs", drugClient.readAll() );
        return "drugs/list";
    }

    @PostMapping("/delete")
    public String delete(@RequestParam Long id, Model model) {
        drugClient.setCurrentDrug(id);
        drugClient.deleteOne();
        model.addAttribute("drugs", drugClient.readAll() );
        return "drugs/list";
    }

    @GetMapping("/add")
    public String addDrug (Model model) {
        model.addAttribute("drugModel", new DrugModel());
        return "drugs/add";
    }


    @PostMapping("/add")
    public String addDrugSubmit (@ModelAttribute DrugModel data, Model model) {

        try {
            DrugDto drug = drugClient.create( DrugConverter.drugModelToDrugSimpleDto( data ) );
            model.addAttribute("success", true);
            model.addAttribute("successMessage", "Drug successfully created with ID: " + drug.getId());
            model.addAttribute("drugModel", drug);
        }
        catch (Exception e) {
            model.addAttribute("success", false);
            model.addAttribute("errorMessage", e.getMessage());
            model.addAttribute("drugModel", data);
        }
        return "drugs/add";
    }

    @GetMapping("/detail")
    public String detail(@RequestParam Long id, Model model) {
        drugClient.setCurrentDrug(id);
        model.addAttribute("drug", drugClient.readOne());
        return "drugs/detail";
    }

    @GetMapping("/edit")
    public String editDrug(@RequestParam Long id, Model model){
        drugClient.setCurrentDrug(id);
        model.addAttribute("drugModel", new DrugModel());
        model.addAttribute("drug", drugClient.readOne());
        return "drugs/edit";
    }

    @PostMapping("/edit")
    public String editDrugSubmit(@RequestParam Long id, @ModelAttribute DrugModel data, Model model) {

        drugClient.setCurrentDrug(id);
        try {
            drugClient.updateOne( DrugConverter.drugModelToDrugSimpleDto( data ) );
            model.addAttribute("success", true);
            model.addAttribute("successMessage", "Drug successfully saved!");
        }
        catch (Exception e) {
            model.addAttribute("success", false);
            model.addAttribute("errorMessage", "There was a problem saving the drug!");
        }

        model.addAttribute("drug", drugClient.readOne());

        return "drugs/detail";
    }

}
