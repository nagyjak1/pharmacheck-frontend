package cz.cvut.fit.tjv.pharmacheck.frontend.api_client;

import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drug.DrugDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drug.DrugSimpleDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drugstore.DrugstoreDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drugstore.DrugstoreSimpleDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.prescription.PrescriptionDto;
import org.glassfish.jersey.logging.LoggingFeature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;

@Component
public class DrugstoreClient {
    private final WebTarget drugstoresEndpoint;
    private final WebTarget singleEndpointTemplate;
    private WebTarget singleDrugstoreEndpoint;

    public DrugstoreClient(@Value("${backend.url}") String apiUrl) {
        var c = ClientBuilder.newClient().register(LoggingFeature.builder().level(Level.ALL).build());
        drugstoresEndpoint = c.target(apiUrl + "/drugstores");
        singleEndpointTemplate = drugstoresEndpoint.path("/{id}");
    }

    public Collection<DrugstoreDto> readAll() {
        var res = drugstoresEndpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .get(DrugstoreDto[].class);
        return Arrays.asList(res);
    }

    public DrugstoreDto readOne() {
        return singleDrugstoreEndpoint.request(MediaType.APPLICATION_JSON_TYPE).get(DrugstoreDto.class);
    }

    public DrugstoreDto create(DrugstoreSimpleDto e) {
        return drugstoresEndpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(e, MediaType.APPLICATION_JSON_TYPE), DrugstoreDto.class);
    }

    public void setCurrentDrugstore(Long id) {
        singleDrugstoreEndpoint = singleEndpointTemplate.resolveTemplate("id", id);
    }

    public void deleteOne() {
        singleDrugstoreEndpoint.request(MediaType.APPLICATION_JSON_TYPE).delete();
    }

    public void updateOne(DrugstoreSimpleDto e) {
        singleDrugstoreEndpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .put(Entity.entity(e, MediaType.APPLICATION_JSON_TYPE), DrugstoreSimpleDto.class);
    }

    public void addDrugToDrugstore(Long drugID) {
        WebTarget endpoint = singleDrugstoreEndpoint.path("/drugs/{drugID}");
        endpoint = endpoint.resolveTemplate("drugID", drugID);
        endpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .header(HttpHeaders.CONTENT_LENGTH, 0)
                .put(Entity.text(""), Object.class);
    }

    public void removeDrugFromDrugstore(Long drugID) {
        WebTarget endpoint = singleDrugstoreEndpoint.path("/drugs/{drugID}");
        endpoint = endpoint.resolveTemplate("drugID", drugID);
        endpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .delete();
    }

}
