package cz.cvut.fit.tjv.pharmacheck.frontend.web.controller;


import cz.cvut.fit.tjv.pharmacheck.frontend.api_client.DrugClient;
import cz.cvut.fit.tjv.pharmacheck.frontend.api_client.DrugstoreClient;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drugstore.DrugstoreDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.web.model.DrugstoreModel;
import cz.cvut.fit.tjv.pharmacheck.frontend.web.model.converter.DrugstoreConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/drugstores")
public class DrugstoreWebController {

    private final DrugstoreClient drugstoreClient;

    private final DrugClient drugClient;

    public DrugstoreWebController(DrugstoreClient drugstoreClient, DrugClient drugClient) {
        this.drugstoreClient = drugstoreClient;
        this.drugClient = drugClient;
    }

    @GetMapping
    public String list(Model model) {
        model.addAttribute("drugstores", drugstoreClient.readAll() );
        return "drugstores/list";
    }

    @GetMapping("/detail")
    public String detail(@RequestParam Long id, Model model) {
        drugstoreClient.setCurrentDrugstore(id);
        model.addAttribute("drugstore", drugstoreClient.readOne());
        return "drugstores/detail";
    }

    @PostMapping("/delete")
    public String delete(@RequestParam Long id, Model model) {
        drugstoreClient.setCurrentDrugstore(id);
        drugstoreClient.deleteOne();
        model.addAttribute("drugstores", drugstoreClient.readAll() );
        return "drugstores/list";
    }

    @GetMapping("/add")
    public String addDrugstore(Model model) {
        model.addAttribute("drugstoreModel", new DrugstoreModel());
        return "drugstores/add";
    }


    @PostMapping("/add")
    public String addDrugstoreSubmit(@ModelAttribute DrugstoreModel data, Model model) {

        DrugstoreConverter convertor = new DrugstoreConverter();
        try {
            DrugstoreDto drugstore = drugstoreClient.create( convertor.drugstoreModelToDrugstoreSimpleDto( data ) );
            model.addAttribute("success", true);
            model.addAttribute("successMessage", "Drugstore successfully created with ID: " + drugstore.getId());
            model.addAttribute("drugstoreModel", drugstore);
        }
        catch (Exception e) {
            model.addAttribute("success", false);
            model.addAttribute("errorMessage", e.getMessage());
            model.addAttribute("drugstoreModel", data);
        }
        return "drugstores/add";
    }

    @GetMapping("/edit")
    public String editOne(@RequestParam Long id, Model model) {
        drugstoreClient.setCurrentDrugstore( id );
        model.addAttribute("drugstoreModel", new DrugstoreModel());
        model.addAttribute( "drugstore", drugstoreClient.readOne() );
        return "drugstores/edit";
    }

    @PostMapping("/edit")
    public String editOneSubmit(@RequestParam Long id, @ModelAttribute DrugstoreModel data, Model model ) {

        drugstoreClient.setCurrentDrugstore(id);

        try {
            drugstoreClient.updateOne( DrugstoreConverter.drugstoreModelToDrugstoreSimpleDto( data ) );
            model.addAttribute("success", true);
            model.addAttribute("successMessage", "Drugstore successfully saved!");
        }
        catch (Exception e) {
            model.addAttribute("success", false);
            model.addAttribute("errorMessage", "There was a problem saving the drugstore!");
        }

        model.addAttribute( "drugstore", drugstoreClient.readOne() );
        return "drugstores/detail";
    }

    @GetMapping("/addDrugs")
    public String addDrugs(@RequestParam Long id, Model model) {
        drugstoreClient.setCurrentDrugstore(id);
        model.addAttribute("drugstore", drugstoreClient.readOne());
        model.addAttribute("allDrugs", drugClient.readAll());
        return "drugstores/addDrugs";
    }

    @PostMapping("/addDrugs")
    public String addDrugsSubmit(@RequestParam Long id, @RequestParam Long drugID, Model model) {
        drugstoreClient.setCurrentDrugstore(id);

        try {
            drugstoreClient.addDrugToDrugstore(drugID);
            model.addAttribute("drugstore", drugstoreClient.readOne());
            model.addAttribute("success", true);
            model.addAttribute("successMessage", "Drug successfully added!");
        } catch ( Exception e) {
            model.addAttribute("drugstore", drugstoreClient.readOne());
            model.addAttribute("success", false);
            model.addAttribute("errorMessage", "There was a problem adding the drug! " + e.getMessage() );
        }
        return "drugstores/detail";
    }

    @PostMapping("/removeDrugs")
    public String removeDrugs(@RequestParam Long id, @RequestParam Long drugID, Model model) {
        drugstoreClient.setCurrentDrugstore(id);
        drugstoreClient.removeDrugFromDrugstore(drugID);
        model.addAttribute( "drugstore", drugstoreClient.readOne());
        return "drugstores/detail";
    }

}
