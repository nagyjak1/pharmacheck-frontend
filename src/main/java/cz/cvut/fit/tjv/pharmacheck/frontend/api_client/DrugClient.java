package cz.cvut.fit.tjv.pharmacheck.frontend.api_client;

import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drug.DrugDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drug.DrugSimpleDto;
import org.glassfish.jersey.logging.LoggingFeature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;

@Component
public class DrugClient {

    private final WebTarget drugsEndpoint;
    private final WebTarget singleEndpointTemplate;
    private WebTarget singleDrugEndpoint;

    public DrugClient(@Value("${backend.url}") String apiUrl) {
        var c = ClientBuilder.newClient().register(LoggingFeature.builder().level(Level.ALL).build());
        drugsEndpoint = c.target(apiUrl + "/drugs");
        singleEndpointTemplate = drugsEndpoint.path("/{id}");
    }

    public Collection<DrugDto> readAll() {
        var res = drugsEndpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .get(DrugDto[].class);
        return Arrays.asList(res);
    }

    public DrugDto create(DrugSimpleDto e) {
        return drugsEndpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(e, MediaType.APPLICATION_JSON_TYPE), DrugDto.class);
    }

    public void setCurrentDrug(long id) {
        singleDrugEndpoint = singleEndpointTemplate.resolveTemplate("id", id);
    }

    public void deleteOne() {
        singleDrugEndpoint.request(MediaType.APPLICATION_JSON_TYPE).delete();
    }

    public DrugDto readOne() {
        return singleDrugEndpoint.request(MediaType.APPLICATION_JSON_TYPE).get(DrugDto.class);
    }

    public void updateOne(DrugSimpleDto e) {
        singleDrugEndpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .put(Entity.entity(e, MediaType.APPLICATION_JSON_TYPE), DrugDto.class);
    }

}