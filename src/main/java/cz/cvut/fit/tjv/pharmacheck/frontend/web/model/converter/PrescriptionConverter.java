package cz.cvut.fit.tjv.pharmacheck.frontend.web.model.converter;

import cz.cvut.fit.tjv.pharmacheck.frontend.domain.prescription.PrescriptionSimpleDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.web.model.PrescriptionModel;

public class PrescriptionConverter {

    public PrescriptionSimpleDto convert(PrescriptionModel model ) {
        return new PrescriptionSimpleDto();
    }
}
