package cz.cvut.fit.tjv.pharmacheck.frontend.web.controller;

import cz.cvut.fit.tjv.pharmacheck.frontend.api_client.DrugClient;
import cz.cvut.fit.tjv.pharmacheck.frontend.api_client.DrugstoreClient;
import cz.cvut.fit.tjv.pharmacheck.frontend.api_client.PrescriptionClient;
import cz.cvut.fit.tjv.pharmacheck.frontend.api_client.UserClient;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drug.DrugDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drug.DrugWithoutPrescriptionDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.prescription.PrescriptionDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.web.model.PrescriptionModel;
import cz.cvut.fit.tjv.pharmacheck.frontend.web.model.converter.PrescriptionConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/prescriptions")
public class PrescriptionWebController {

    private final PrescriptionClient prescriptionClient;
    private final UserClient userClient;
    private final DrugClient drugClient;

    public PrescriptionWebController(PrescriptionClient prescriptionClient, UserClient userClient, DrugClient drugClient) {
        this.prescriptionClient = prescriptionClient;
        this.userClient = userClient;
        this.drugClient = drugClient;
    }

    @GetMapping
    public String list(Model model) {
        model.addAttribute("prescriptions", prescriptionClient.readAll() );
        return "prescriptions/list";
    }

    @PostMapping("/delete")
    public String delete(@RequestParam Long id, Model model) {
        prescriptionClient.setCurrentPrescription(id);
        prescriptionClient.deleteOne();
        model.addAttribute("prescriptions", prescriptionClient.readAll() );
        return "prescriptions/list";
    }

    @GetMapping("/add")
    public String addPrescription(Model model) {
        model.addAttribute("prescriptionModel", new PrescriptionModel());
        return "prescriptions/add";
    }


    @PostMapping("/add")
    public String addPrescriptionSubmit(Model model) {
        try {
            PrescriptionDto prescription = prescriptionClient.create();
            model.addAttribute("success", true);
            model.addAttribute("successMessage", "Prescription successfully created with ID: " + prescription.getId());
        }
        catch (Exception e) {
            model.addAttribute("success", false);
            model.addAttribute("errorMessage", e.getMessage());
        }

        model.addAttribute("prescriptions", prescriptionClient.readAll());
        return "prescriptions/list";
    }

    @GetMapping("/detail")
    public String detail(@RequestParam Long id, Model model) {
        prescriptionClient.setCurrentPrescription(id);
        model.addAttribute("prescription", prescriptionClient.readOne());
        model.addAttribute("drugstores", prescriptionClient.readDrugstoresFromPrescription() );
        return "prescriptions/detail";
    }


    @GetMapping("/changeUser")
    public String changeUser(@RequestParam Long id, Model model) {
        prescriptionClient.setCurrentPrescription(id);
        model.addAttribute("prescription", prescriptionClient.readOne());
        model.addAttribute("allUsers", userClient.readAll());
        return "prescriptions/changeUser";
    }

    @PostMapping("/changeUser")
    public String changeUserSubmit(@RequestParam Long id, @RequestParam Long userID, Model model) {
        prescriptionClient.setCurrentPrescription(id);
        prescriptionClient.changeUser(userID);
        model.addAttribute( "prescription", prescriptionClient.readOne() );
        model.addAttribute("drugstores", prescriptionClient.readDrugstoresFromPrescription() );

        return "prescriptions/detail";
    }

    @PostMapping("/removeUser")
    public String removeUser(@RequestParam Long id, Model model) {
        prescriptionClient.setCurrentPrescription(id);
        prescriptionClient.removeUser();
        model.addAttribute("prescription", prescriptionClient.readOne());
        model.addAttribute("drugstores", prescriptionClient.readDrugstoresFromPrescription() );

        return "prescriptions/detail";
    }

    @GetMapping("/addDrugs")
    public String addDrugs(@RequestParam Long id, Model model) {
        prescriptionClient.setCurrentPrescription(id);
        model.addAttribute("prescription", prescriptionClient.readOne());
        model.addAttribute("allDrugs", drugClient.readAll() );
        return "prescriptions/addDrugs";
    }

    @PostMapping("/addDrugs")
    public String addDrugsSubmit(@RequestParam Long id, @RequestParam Long drugID, Model model) {
        prescriptionClient.setCurrentPrescription(id);

        try {
            prescriptionClient.addDrugToPrescription(drugID);
            model.addAttribute("prescription", prescriptionClient.readOne());
            model.addAttribute("success", true);
            model.addAttribute("successMessage", "Drug successfully added!");
        } catch (Exception e) {
            model.addAttribute("prescription", prescriptionClient.readOne());
            model.addAttribute("success", false);
            model.addAttribute("errorMessage", "There was a problem adding the drug! " + e.getMessage() );
        }

        model.addAttribute("drugstores", prescriptionClient.readDrugstoresFromPrescription() );
        return "prescriptions/detail";
    }

    @PostMapping("/removeDrugs")
    public String removeDrugs(@RequestParam Long id, @RequestParam Long drugID, Model model) {
        prescriptionClient.setCurrentPrescription(id);
        prescriptionClient.removeDrugFromPrescription(drugID);

        model.addAttribute("prescription", prescriptionClient.readOne());
        model.addAttribute("drugstores", prescriptionClient.readDrugstoresFromPrescription() );

        return "prescriptions/detail";
    }

}
