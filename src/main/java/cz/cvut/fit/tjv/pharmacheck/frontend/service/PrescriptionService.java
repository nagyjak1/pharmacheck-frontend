package cz.cvut.fit.tjv.pharmacheck.frontend.service;

import cz.cvut.fit.tjv.pharmacheck.frontend.api_client.PrescriptionClient;
import org.springframework.stereotype.Service;

@Service
public class PrescriptionService {

    private final PrescriptionClient prescriptionClient;

    public PrescriptionService(PrescriptionClient prescriptionClient) {
        this.prescriptionClient = prescriptionClient;
    }
}
