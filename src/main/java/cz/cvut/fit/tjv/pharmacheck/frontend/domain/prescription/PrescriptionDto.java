package cz.cvut.fit.tjv.pharmacheck.frontend.domain.prescription;

import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drug.DrugWithoutPrescriptionDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.user.UserWithoutPrescriptionDto;

import java.util.Collection;
import java.util.Objects;
import java.util.UUID;

public class PrescriptionDto {

    private Long id;

    private UUID EPrescriptionCode;

    private UserWithoutPrescriptionDto user;

    private Collection<DrugWithoutPrescriptionDto> drugs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserWithoutPrescriptionDto getUser() {
        return user;
    }

    public void setUser(UserWithoutPrescriptionDto user) {
        this.user = user;
    }

    public Collection<DrugWithoutPrescriptionDto> getDrugs() {
        return drugs;
    }

    public void setDrugs(Collection<DrugWithoutPrescriptionDto> drugs) {
        this.drugs = drugs;
    }

    public UUID getEPrescriptionCode() {
        return EPrescriptionCode;
    }

    public void setEPrescriptionCode(UUID EPrescriptionCode) {
        this.EPrescriptionCode = EPrescriptionCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrescriptionDto that = (PrescriptionDto) o;
        return Objects.equals(id, that.id) && Objects.equals(EPrescriptionCode, that.EPrescriptionCode) && Objects.equals(user, that.user) && Objects.equals(drugs, that.drugs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, EPrescriptionCode, user, drugs);
    }
}
