package cz.cvut.fit.tjv.pharmacheck.frontend.web.model;

public class DrugstoreModel {

    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
