package cz.cvut.fit.tjv.pharmacheck.frontend.service;

import cz.cvut.fit.tjv.pharmacheck.frontend.api_client.UserClient;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserClient userClient;

    public UserService(UserClient userClient) {
        this.userClient = userClient;
    }
}
