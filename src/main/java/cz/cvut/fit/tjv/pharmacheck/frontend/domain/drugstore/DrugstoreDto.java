package cz.cvut.fit.tjv.pharmacheck.frontend.domain.drugstore;

import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drug.DrugWithoutDrugstoreDto;

import java.util.Collection;
import java.util.Objects;

public class DrugstoreDto {

    private Long id;

    private Collection<DrugWithoutDrugstoreDto> drugs;

    private String city;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collection<DrugWithoutDrugstoreDto> getDrugs() {
        return drugs;
    }

    public void setDrugs(Collection<DrugWithoutDrugstoreDto> drugs) {
        this.drugs = drugs;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrugstoreDto that = (DrugstoreDto) o;
        return Objects.equals(id, that.id) && Objects.equals(drugs, that.drugs) && Objects.equals(city, that.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, drugs, city);
    }
}
