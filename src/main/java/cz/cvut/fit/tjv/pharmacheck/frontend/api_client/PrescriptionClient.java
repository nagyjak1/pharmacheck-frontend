package cz.cvut.fit.tjv.pharmacheck.frontend.api_client;

import cz.cvut.fit.tjv.pharmacheck.frontend.domain.drugstore.DrugstoreDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.prescription.PrescriptionDto;
import cz.cvut.fit.tjv.pharmacheck.frontend.domain.prescription.PrescriptionSimpleDto;
import org.glassfish.jersey.logging.LoggingFeature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.Collection;
import java.util.logging.Level;

@Component
public class PrescriptionClient {

    private final WebTarget prescriptionsEndpoint;
    private final WebTarget singleEndpointTemplate;
    private WebTarget singlePrescriptionEndpoint;

    public PrescriptionClient(@Value("${backend.url}") String apiUrl) {
        var c = ClientBuilder.newClient().register(LoggingFeature.builder().level(Level.ALL).build());
        prescriptionsEndpoint = c.target(apiUrl + "/prescriptions");
        singleEndpointTemplate = prescriptionsEndpoint.path("/{id}");
    }

    public Collection<PrescriptionDto> readAll() {
        var res = prescriptionsEndpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .get(PrescriptionDto[].class);
        return Arrays.asList(res);
    }

    public PrescriptionDto create() {
        return prescriptionsEndpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.json(null), PrescriptionDto.class);
    }

    public void setCurrentPrescription(long id) {
        singlePrescriptionEndpoint = singleEndpointTemplate.resolveTemplate("id", id);
    }

    public void deleteOne() {
        singlePrescriptionEndpoint.request(MediaType.APPLICATION_JSON_TYPE).delete();
    }

    public PrescriptionDto readOne() {
        return singlePrescriptionEndpoint.request(MediaType.APPLICATION_JSON_TYPE).get(PrescriptionDto.class);
    }

    public void changeUser(Long userID) {
        WebTarget endpoint = singlePrescriptionEndpoint.path("/user/{userID}");
        endpoint = endpoint.resolveTemplate("userID", userID);
        endpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .header(HttpHeaders.CONTENT_LENGTH, 0)
                .put(Entity.text(""));
    }

    public void removeUser() {
        WebTarget endpoint = singlePrescriptionEndpoint.path("/user");
        endpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .delete();
    }

    public void addDrugToPrescription( Long drugID ) {
        WebTarget endpoint = singlePrescriptionEndpoint.path("/drugs/{drugID}");
        endpoint = endpoint.resolveTemplate("drugID", drugID);
        endpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .header(HttpHeaders.CONTENT_LENGTH, 0)
                .put(Entity.text(""), Object.class);
    }

    public void removeDrugFromPrescription( Long drugID) {
        WebTarget endpoint = singlePrescriptionEndpoint.path("/drugs/{drugID}");
        endpoint = endpoint.resolveTemplate("drugID", drugID);
        endpoint.request(MediaType.APPLICATION_JSON_TYPE)
                .delete();
    }


    public Collection<DrugstoreDto> readDrugstoresFromPrescription() {
        WebTarget endpoint = singlePrescriptionEndpoint.path("/drugstores");
        var res = endpoint.request(MediaType.APPLICATION_JSON_TYPE).get(DrugstoreDto[].class);
        return Arrays.asList(res);
    }
}
