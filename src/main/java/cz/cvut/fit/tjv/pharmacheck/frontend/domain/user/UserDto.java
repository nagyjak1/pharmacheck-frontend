package cz.cvut.fit.tjv.pharmacheck.frontend.domain.user;

import cz.cvut.fit.tjv.pharmacheck.frontend.domain.prescription.PrescriptionWithoutUserDto;

import java.util.Collection;
import java.util.Objects;

public class UserDto {

    private Long id;

    private String username;

    private Collection<PrescriptionWithoutUserDto> prescriptions;

    public Collection<PrescriptionWithoutUserDto> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(Collection<PrescriptionWithoutUserDto> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto userDto = (UserDto) o;
        return Objects.equals(id, userDto.id) && Objects.equals(username, userDto.username) && Objects.equals(prescriptions, userDto.prescriptions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, prescriptions);
    }
}
